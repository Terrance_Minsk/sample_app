class ApplicationMailer < ActionMailer::Base
  default from: "PhilippsApp@sample.com"
  layout 'mailer'
end